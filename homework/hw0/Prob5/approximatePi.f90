! Zachary Potter
! zppotter@ucsc.edu
! AMS 213A
! approximatePi.f90
! Approximates the value of pi using a summation and compares it to the real value of Pi.
! Alter the threshold in order to get a more precise value of pi.

! Results
! pi_true = 3.1415926535897931
! For threshold = 1.e-4 : N = 2, diff = 5.2632432114840810E-006, pi_approx = 3.1415873903465816
! For threshold = 1.e-8 : N = 4, diff = 8.1294566633971499E-009, pi_approx = 3.1415926454603365
! For threshold = 1.e-12 : N = 7, diff = 8.2023277059306565E-013, pi_approx = 3.1415926535889729
! For threshold = 1.e-16 : N = 10, diff = 0.0000000000000000, pi_approx = 3.1415926535897931


program approximatePi

	implicit none

	call summation(1.e-4)
	call summation(1.e-8)
	call summation(1.e-12)
	call summation(1.e-16)

end program approximatePi

subroutine summation(threshold)
	implicit none
	real, intent(in) :: threshold
	real :: pi_true, pi_approx, diff, multTerm, sumTerm1, sumTerm2, sumTerm3, sumTerm4
	integer :: i, imax
	
	! compute pi as arc-cosine of -1:
	pi_true = acos(-1.d0)

	pi_approx = 0
	imax = 20
	do i = 0,imax
		multTerm = 1.0/(16.0**i)
		sumTerm1 = 4.0/(8.0*i+1.0)
		sumTerm2 = 2.0/(8.0*i+4.0)
		sumTerm3 = 1.0/(8.0*i+5.0)
		sumTerm4 = 1.0/(8.0*i+6.0)
		pi_approx = pi_approx + (multTerm * (sumTerm1 - sumTerm2 - sumTerm3 - sumTerm4))
		diff = abs(pi_approx - pi_true)
		if (diff <= threshold) exit
	end do
	
	if (i == imax + 1) then
		print *, "Warning: imax iterations reached."
	end if

	print *, CHAR(10)
	print *, "threshold = ", threshold
	print *, "pi_approx = ", pi_approx
	print *, "diff = ", diff
	print *, "numIterations = ", i
end subroutine summation
