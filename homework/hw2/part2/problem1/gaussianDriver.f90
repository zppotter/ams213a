! Zachary Potter
! zppotter@ucsc.edu
! AMS 213A
! gaussianDriver.f90

! OVERVIEW:
! This is a driver program that calls functions from LinAl.f90 in order to perform
! Gaussian Elimination and backsubstitution given matrices A and B to solve for X
! in the classic AX=B equation. 

! COMPILATION INSTRUCTIONS: This driver program comes with a Makefile. Therefore, to
! compile and run the program, type the following lines while in the problem1 directory:
! make
! ./gaussianDriver

! Note: For the program to compile and run properly, it will also need the following files:
! LinAl.f90
! Amat.dat
! Bmat.dat
! Makefile

! PROGRAM INPUTS:
! This program reqires two input matrices: Amat.dat and Bmat.dat, where Amat.dat represents
! the A matrix and Bmat.dat represents the B matrix in the equation AX=B. A is the matrix
! which Gaussian Elimination will be performed on, and B contains n many RHS vectors. The
! structure of the .dat files is:
! 3  4
! 1.2     1.3     -1.4    3.31
! 31.1    0.1     5.411   -1.23
! -5.4    7.42    10      -17.4
! Note that the first 2 lines are the matrix dimensions, m by n, then the next m lines are
! the matrix entries. Note that entries must be separated by a tab.

! PROGRAM OUTPUTS:
! This program will print several matrices to the console. First, it will print the given
! A and B matrices. Then, it will perform Gaussian Elimination and print the resulting
! upper triangular A matrix and the modified B matrix. Next, it will perform backsubstitution
! and then print the resulting X matrix, which is the solution to AX=B. Then, it will
! calculate the error matrix, as well as the norm of each column of the error matrix and
! print the matrix and norms to the console. 

program gaussianDriver
	use LinAl, only: readAndAllocateMat, printMat, trace, euclideanNorm, gaussianElimination, gaussBacksub
	implicit none
	real, dimension(:,:), allocatable :: A, B, As, Bs, E
	integer :: msizeA, nsizeA, msizeB, nsizeB
	logical :: singular
	real :: normResult
	integer :: i
	
	! Read .dat files and allocate matrices ---------------
	call readAndAllocateMat(A, msizeA, nsizeA, "Amat.dat")
	call readAndAllocateMat(B, msizeB, nsizeB, "Bmat.dat")
	allocate(As(msizeA,nsizeA))
	allocate(Bs(msizeB,nsizeB))
	allocate(E(msizeA,nsizeB))
	As = A
	Bs = B
	E = 0.0
	
	! Print original matrices ------------------------------
	print *, "A ="
	call printMat(A, msizeA, nsizeA)
	print *, "B ="
	call printMat(B, msizeB, nsizeB)
	
	! Check to make sure that matrix sizes match before calling gaussianElimination
	if (msizeA /= nsizeA .or. msizeB /= nsizeA) then
		print *, "Error: incorrect size matrices"
		stop
	end if
	
	! Do Gaussian Elimination ------------------------------
	call gaussianElimination(A, B, msizeA, nsizeB, singular)
	if (singular) then 
		print *, "Error: problem is singular"
		stop
	end if
	
	! Print A and B matrices after GE (A becomes U and B becomes Y)
	print *, "U ="
	call printMat(A, msizeA, nsizeA)
	print *, "Y ="
	call printMat(B, msizeB, nsizeB)
	
	! Do Gaussian Backsubstitution -------------------------
	call gaussBacksub(A, B, msizeA, nsizeB, singular)
	if (singular) then 
		print *, "Error: problem is singular"
		stop
	end if
	
	! Print B matrix after gaussBacksub (B has become the desired X matrix)
	print *, "X ="
	call printMat(B, msizeB, nsizeB)
	
	! Calculate the error matrix ---------------------------
	E = matmul(As,B) - Bs
	
	! Print the error matrix -------------------------------
	print *, "E ="
	call printMat(E, msizeB, nsizeB)
	
	! Calculate norm of each column of error matrix to make sure it is less than machine error
	do i = 1, nsizeB
		call euclideanNorm(E(:, i), nsizeB, normResult)
		print *, 'error column ', i, ' norm = ', normResult
	end do
	
	deallocate(A)
	deallocate(B)
	deallocate(As)
	deallocate(Bs)
	deallocate(E)

end program gaussianDriver