! Zachary Potter
! zppotter@ucsc.edu
! AMS 213A
! householderDriver.f90

! OVERVIEW:
! THIS IS MY MAIN HOMEWORK SUBMISSION
! This is a driver program that calls functions from LinAl.f90 in order to perform
! Householder factorization (i.e. a QR decomposition) given matrix A and vector b
! to solve for x in the overdetermined system Ax=b. 

! COMPILATION INSTRUCTIONS: This driver program comes with a Makefile. Therefore, to
! compile and run the program, type the following lines while in the part2 directory:
! make
! ./householderDriver

! Note: For the program to compile and run properly, it will also need the following files:
! LinAl.f90
! atkinson.dat
! Makefile

! PROGRAM INPUTS:
! This program requires one input matrix: atkinson.dat, the first column of the data represents
! x values, and the second column represents y values. x values will be used to compute the A 
! matrix using the Vandermonde method, and the y values will be used as the b column vector. 
! The atkinson.dat file contains 21 lines, and its structure is:
! 0.00	0.486
! 0.05	0.866
! 0.10	0.944
! 0.15	1.144
! ... (etc.) ...
! Note that entries must be separated by a tab.

! PROGRAM OUTPUTS:
! This program will print several matrices to the console. First, it will print the given
! A Vandermonde matrix and the b vector from the atkinson.dat file. Then, it will perform a QR
! Decomposition on A using the Householder method and print the resulting Q and R matrices.
! Next, it will calculate A - QR and its norm, and Q^T Q - I and its norm and print them.
! Finally, it will perform backsubstitution on the system Rx = Q^T b in order to solve for x,
! which will be the same x in the original system Ax = b, and print x. Finally, it will calculate
! Ax - b, where A and b are the original matrices from the atkinson.dat file, and x is the x vector
! computed using the least squares method, and it will print that norm.
! Note: This program uses a polynomial of degree 3 by default. To use a different degree, change
! the value of the degree variable, as well as the value of nsizeA to degree+1. Also, change
! the sizes of the matrices to match nsizeA.

program householderDriver
	use LinAl, only: readAtkinson, printMat, euclideanNorm, householderFactorization, gaussBacksub, frobeniusNorm
	implicit none
	integer :: msize = 21, degree = 3, nsizeA = 4
	real, dimension(21,4) :: A, As, Q
    real, dimension(4,4) :: Ident
	real, dimension(21) :: b, bs
	real, dimension(4) :: x
	real :: normResult
    logical :: singular
	integer :: i, j
	
	! Read .dat file and allocate matrices ---------------
	call readAtkinson(A, b, msize, nsizeA, "atkinson.dat")
    As = A
    bs = b
    
    print *, "A ="
    call printMat(A, msize, nsizeA)
    print *, "b ="
	call printMat(b, msize, 1)
    
    ! Compute householderFactorization. Note: A is transformed into R, and b is transformed into Q^T b
    call householderFactorization(A, Q, b, msize, nsizeA)
    
    print *, "R ="
	call printMat(A(1:nsizeA, :), nsizeA, nsizeA)
    print *, "Q ="
	call printMat(Q, msize, nsizeA)
    
    ! Compute and print A - QR
    print *, "A - QR ="
	call printMat(As - matmul(Q, A(1:nsizeA, :)), msize, nsizeA)
    call frobeniusNorm(As - matmul(Q, A(1:nsizeA, :)), msize, nsizeA, normResult)
    print *, "norm of A - QR = ", normResult
    
    Ident = 0
    do i = 1, nsizeA ! Create identity matrix
        Ident(i,i) = 1
    end do
    print *, "Q^T Q - I ="
	call printMat(matmul(transpose(Q),Q) - Ident, nsizeA, nsizeA) ! Print result of Q^T Q - I
    call frobeniusNorm(matmul(transpose(Q),Q) - Ident, nsizeA, nsizeA, normResult)
    print *, "norm of Q^T Q - I = ", normResult
    
    ! Print Q^T b, which is calculated in householderFactorization function
    !print *, "Q^T b ="
    !call printMat(b, msize, 1)
    
    ! Do gaussian backsubstitution to find x. Note: b vector is transformed into x by backsub method.
    ! When using x, the entries x(1:nsizeA) are the polynomial coefficients, and the other entries are
    ! the residuals.
    call gaussBacksub(A(1:nsizeA, :), b, nsizeA, 1, singular)
    print *, "x ="
    call printMat(b, msize, 1)
    
    ! Compute b by multiplying original A by computed x
    print *, "Ax = "
    b = matmul(As, b(1:nsizeA))
    call printMat(b, msize, 1)
    
    ! Calculate norm of original b minus computed b (bs - b)
    call euclideanNorm(bs - b, msize, normResult)
    print *, "error norm = ", normResult
	
end program householderDriver