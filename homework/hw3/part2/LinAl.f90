! Zachary Potter
! zppotter@ucsc.edu
! AMS 213A
! LinAl.f90
! This is a module file that contains functions that perform Gaussian Elimination,
! LU decomposition, backsubstitution, and other necessary functions to perform
! those operations.
! This is a library that should be compiled with a driver program, e.g. gaussianDriver.f90
! or luDriver.f90

module LinAl
implicit none

contains

!********************************************************

subroutine readAndAllocateMat(mat,msize,nsize,filename)

character(len = *) filename
real, dimension(:,:), allocatable, intent(in out) :: mat
integer :: msize, nsize

integer :: i,j

! Reads a file containing the matrix A 
! Sample file:
! 3  4
! 1.2     1.3     -1.4    3.31
! 31.1    0.1     5.411   -1.23
! -5.4    7.42    10      -17.4
! Note that the first 2 lines are the matrix dimensions, 
! then the next msize lines are the matrix entries
! Note that entries must be separated by a tab.
! Then allocates an array of size (msize,nsize), populates the matrix,
! and returns the array. 

! This routine takes as INPUTS:
! filename = a character string with the name of file to read
! This routine returns as OUTPUTS:
! msize = first dimension of read matrix
! nsize = second dimension of read matrix
! mat = read matrix. Note that mat type is real.

open(10,file=filename)

! Read the matrix dimensions
read(10,*) msize,nsize

! Allocate matrix
allocate(mat(msize,nsize))

! Read matrix
do i=1,msize
   read(10,*) ( mat(i,j), j=1,nsize )
enddo

close(10)

end subroutine readandallocatemat



subroutine printMat(mat, msize, nsize)

! Prints out a matrix and it's dimensions

! This routine takes as INPUTS:
! mat = the matrix to print
! msize = first dimension of matrix
! nsize = second dimension of matrix
! This routine has no OUTPUT variables

integer, intent(in) :: msize, nsize
real, dimension(msize,nsize), intent(in) :: mat
integer :: i

print *, msize, 'by ', nsize, 'matrix:' 
do i = 1, msize
	print *, mat(i, :)
end do

end subroutine printMat



subroutine trace(mat, msize, res)

! Calculates the trace of the given square matrix

! This routine takes as INPUTS:
! mat = the matrix whose trace will be calculated
! msize = dimension of matrix. Note: matrix must be square
! This routine returns as OUTPUTS:
! res = the result (the trace of mat)

integer, intent(in) :: msize
real, dimension(msize,msize), intent(in) :: mat
real, intent(in out) :: res
integer :: i

res = 0
do i = 1, msize
	res = res + mat(i, i)
end do

end subroutine trace



subroutine euclideanNorm(vec, length, res)

! Calculates the Euclidean norm of the given vector

! This routine takes as INPUTS:
! vec = the vector whose norm will be calculated
! length = dimension of vector
! This routine returns as OUTPUTS:
! res = the result (the norm of vec)

integer, intent(in) :: length
real, dimension(length), intent(in) :: vec
real, intent(out) :: res

res = 0
res = sqrt(sum(vec(:length) * vec(:length)))

end subroutine euclideanNorm



subroutine gaussianElimination(A, B, msize, nsize, singular)

! Performs Gaussian Elimination with partial pivoting on matrix A with 
! corresponding operations on a matrix B containing n RHS vectors.
! Note: this algorithm is adapted from Prof. Lee's algorithm in his notes

! This routine takes as INPUTS:
! A = the m by m square matrix to perform Gaussian elimination on
! B = an m by n matrix which contains n many RHS vectors of dimension m
! msize = dimension of square matrix A and first dimension of B
! nsize = second dimension of matrix B
! This routine returns as OUTPUTS:
! singular = a logical variable indicating if the problem Ax = B is singular
! A = this input matrix will be used as output. it will become upper triangular
! B = the modified set of RHS vectors

integer, intent(in) :: msize, nsize
real, dimension(msize,msize), intent(in out) :: A
real, dimension(msize,nsize), intent(in out) :: B
logical, intent(out) :: singular
integer :: i, j, p
real :: columnMax, mult, div
real, dimension(msize) :: tmp
real, dimension(nsize) :: tmp2

singular = .false. ! Assume matrix is not singular
do j = 1, msize-1
	columnMax = A(j,j)
	p = j
	do i = j+1, msize ! Find the pivot index in this loop
		if (A(i,j) > columnMax) then
			columnMax = A(i,j)
			p = i
		end if
	end do
	
	if (p /= j) then ! Swap rows if needed
		tmp = A(j, :)
		A(j, :) = A(p, :)
		A(p, :) = tmp
		tmp2 = B(j, :)
		B(j, :) = B(p, :)
		B(p, :) = tmp2
	end if
	
	if (A(j,j) == 0) then ! Matrix is singular, exit subroutine
		print *, "Singular!"
		singular = .true.
		return
	end if
	
	div = A(j,j)
	do i = j+1, msize ! Loop over rows below row j
		mult = A(i,j)
		A(i, :) = A(i, :) - ((mult * A(j, :)) / div)
		B(i, :) = B(i, :) - ((mult * B(j, :)) / div)
	end do
end do

end subroutine gaussianElimination



subroutine gaussBacksub(U, B, msize, nsize, singular)

! Performs Gaussian backsubsitution given upper triangular matrix U,
! and a matrix B containing n RHS vectors. The result is returned in B.
! Note: this algorithm is adapted from Prof. Lee's algorithm in his notes

! This routine takes as INPUTS:
! U = the m by m square upper triangular matrix to perform backsubsitution on
! B = an m by n matrix which contains n many RHS vectors of dimension m
! msize = dimension of square matrix A and first dimension of B
! nsize = second dimension of matrix B
! This routine returns as OUTPUTS:
! B = now contains X, the solution to AX=B
! singular = a logical variable indicating if the problem Ax = B is singular

integer, intent(in) :: msize, nsize
real, dimension(msize,msize), intent(in) :: U
real, dimension(msize,nsize), intent(in out) :: B
logical, intent(out) :: singular
integer :: i, k
real, dimension(nsize) :: total

singular = .false. ! Assume matrix is not singular
if (U(msize,msize) == 0) then ! Matrix is singular, exit subroutine
	print *, "Singular!"
	singular = .true.
	return
end if

B(msize, :) = B(msize, :) / U(msize, msize)

do i = msize-1, 1, -1
	if (U(i,i) == 0) then ! Matrix is singular, exit subroutine
		print *, "Singular!"
		singular = .true.
		return
	end if
	
	total = 0.0
	do k = i+1, msize
		total = total + U(i,k)*B(k, :)
	end do
	
	B(i, :) = (B(i, :) - total)/U(i,i)
end do

end subroutine gaussBacksub



subroutine luDecomposition(A, msize, singular, s)

! Performs LU decomposition with partial pivoting on matrix A with 
! corresponding permutation operations on vector s.
! Note: this algorithm is adapted from Prof. Lee's algorithm in his notes

! This routine takes as INPUTS:
! A = the m by m square matrix to perform LU decomposition on
! msize = dimension of square matrix A and length of s
! This routine returns as OUTPUTS:
! singular = a logical variable indicating if the problem Ax = B is singular
! A = this input matrix will be transformed into the L matrix.
! s = a length m permutation vector

integer, intent(in) :: msize
real, dimension(msize,msize), intent(in out) :: A
integer, dimension(msize), intent(out) :: s
logical, intent(out) :: singular
integer :: i, j, k, p
real :: columnMax, tmp2
real, dimension(msize) :: tmp

do j=1, msize
	s(j) = j
end do

singular = .false. ! Assume matrix is not singular
do j = 1, msize
	columnMax = A(j,j)
	p = j
	do i = j+1, msize ! Find the pivot index in this loop
		if (A(i,j) > columnMax) then
			columnMax = A(i,j)
			p = i
		end if
	end do
	
	if (p /= j) then ! Swap rows if needed and record permutation
		tmp = A(j, :)
		A(j, :) = A(p, :)
		A(p, :) = tmp
		tmp2 = s(j)
		s(j) = s(p)
		s(p) = tmp2
	end if
	
	if (A(j,j) == 0) then ! Matrix is singular, exit subroutine
		print *, "Singular!"
		singular = .true.
		return
	end if
	
	do i = j+1, msize 
		A(i, j) = A(i, j) / A(j, j)
		! create l_ij and store them in a_ij
		do k = j+1, msize
			A(i, k) = A(i, k) - A(i, j)*A(j, k)
		end do
	! Updates A
	end do
end do

end subroutine luDecomposition



subroutine luBacksub(LU, B, msize, nsize, s, singular)

! Performs LU backsubstitution given a decomposed matrix LU,
! and a matrix B containing n RHS vectors. The result is returned in B.
! Note: this algorithm is adapted from Prof. Lee's algorithm in his notes

! This routine takes as INPUTS:
! LU = the m by m square upper decomposed LU matrix to perform backsubsitution on
! B = an m by n matrix which contains n many RHS vectors of length m
! msize = dimension of square matrix A, vector s, and first dimension of B
! nsize = second dimension of matrix B
! This routine returns as OUTPUTS:
! B = now contains X, the solution to AX=B
! singular = a logical variable indicating if the problem Ax = B is singular

integer, intent(in) :: msize, nsize
real, dimension(msize,msize), intent(in) :: LU
real, dimension(msize,nsize), intent(in out) :: B
integer, dimension(msize), intent(in) :: s
logical, intent(out) :: singular
integer :: i, j, k
real, dimension(nsize) :: total
real, dimension(msize,nsize) :: Y

do j = 1, msize ! Initialize y with Pb
	Y(j, :) = B(s(j), :)
end do

do j=1, msize-1 ! Forward substitution
! Do y=M_j y
	do i = j+1, msize
		Y(i, :) = Y(i, :) - Y(j, :)*LU(i, j)
	end do
end do

singular = .false. ! Assume matrix is not singular

do i = msize, 1, -1 ! Backward substitution
	if (LU(i,i) == 0) then ! Matrix is singular, exit subroutine
		print *, "Singular!"
		singular = .true.
		return
	end if
	
	total = 0.0
	do k = i+1, msize
		total = total + LU(i,k)*B(k, :)
	end do
	
	B(i, :) = (Y(i, :) - total)/LU(i,i)
end do

end subroutine luBacksub



subroutine readAtkinson(A,b,msize,nsizeA,filename)

character(len = *) filename
integer, intent(in) :: msize, nsizeA
real, dimension(msize,nsizeA), intent(out) :: A
real, dimension(msize), intent(out) :: b
integer :: i

! Reads the file atkinson.dat for homework 3 and computes the 
! Vandermonde matrix A and the corresponding b vector.
! The atkinson.dat file contains 21 x and y coordinates.
! The x column is used to compute the Vandermonde 
! matrix A, and the y column is returned in the b argument.

! This routine takes as INPUTS:
! filename = a character string with the name of file to read
! msize = the number of lines to read (this will be the first dimension of A and the length of b)
! nsize = the second dimension of A. This will determine the degree of the Vandermonde matrix.
! This routine returns as OUTPUTS:
! A = the computed Vandermonde matrix of size msize by nsize
! b = the vector of length msize read from the file

open(10,file=filename)

! Read matrix
do i=1,msize
	read(10,*) A(i,2), b(i)
enddo

! Build A matrix
A(:, 1) = 1
do i = 3, nsizeA
	A(:, i) = A(:, 2)**(i-1)
end do

close(10)

end subroutine readAtkinson



subroutine householderFactorization(A, Q, b, msize, nsize)

! Performs Householder Factorization on matrix A to compute a 
! QR decomposition of A. 
! Note: this algorithm is adapted from Prof. Lee's algorithm in his notes

! This routine takes as INPUTS:
! A = the m by n matrix to perform Householder QR Factorization on
! b = The vector corresponding to A in the equation Ax = b. Is of length msize
! msize = first dimension of matrix A and Q and the length of b
! nsize = second dimension of matrix A and Q
! This routine returns as OUTPUTS:
! A = this input matrix will be transformed into the upper triangular R matrix.
! Q = this will hold the corresponding Q matrix
! b = this will be updated to be Q^T b (i.e. H_n * ... * H_1 b)

integer, intent(in) :: msize, nsize
real, dimension(msize,nsize), intent(in out) :: A, Q
real, dimension(msize), intent(in out) :: b
real, dimension(msize,nsize) :: V, sub, Ident
real, dimension(msize) :: v_j, sub_b
real, dimension(nsize) :: tmp
real :: norm, s_j, sgn, tmp_b
integer :: i, j

V = 0
sub = 0
Q = 0
do j = 1, nsize ! initialize Q as identity matrix
    Q(j,j) = 1
end do

do j = 1, nsize
    sgn = sign(1.0, A(j,j)) ! find sign of A_jj
    call euclideanNorm(A(j:msize, j), msize - j + 1, norm)
    s_j = norm*sgn ! compute signed norm
        
    v_j = 0
    v_j(j) = A(j,j) + s_j
    do i = j+1, msize
        v_j(i) = A(i,j)
    end do
    call euclideanNorm(v_j, msize, norm)
    v_j = v_j / norm ! compute Householder vector
    
    V(:,j) = v_j ! save off v_j vector into big V matrix
    
    tmp = 0
    sub = 0
    do i = j, nsize ! Apply each H to A
        tmp(i) = sum(v_j*A(:,i))
        sub(:, i) = v_j*tmp(i)
    end do
    A = A - 2*sub ! Update A
    
    ! Apply each H to b
    tmp_b = sum(v_j*b)
    sub_b = v_j*tmp_b
    
    b = b - 2*sub_b ! Update b
end do

! Calculate Q matrix
do j = nsize, 1, -1
    tmp = 0
    sub = 0
    do i = j, nsize
        tmp(i) = sum(V(:,j)*Q(:,i))
        sub(:, i) = V(:,j)*tmp(i)
    end do
    Q = Q - 2*sub ! Update Q
end do

end subroutine householderFactorization



subroutine frobeniusNorm(mat, msize, nsize, res)

! Calculates the Frobenius norm of the given matrix

! This routine takes as INPUTS:
! mat = the matrix whose norm will be calculated
! msize = first dimension of the matrix
! nsize = second dimension of the matrix
! This routine returns as OUTPUTS:
! res = the result (the norm of mat)

integer, intent(in) :: msize, nsize
real, dimension(msize,msize), intent(in) :: mat
real, intent(out) :: res
integer :: i, j

res = 0
do i = 1, msize
    do j = 1, nsize
        res = res + (mat(i,j)*mat(i,j))
    end do
end do

res = sqrt(res)

end subroutine frobeniusNorm

end module LinAl
