! Zachary Potter
! zppotter@ucsc.edu
! AMS 213A
! houseExampleDriver.f90

! OVERVIEW:
! THIS IS A SECONDARY SUBMISSION (USED FOR DEMONSTRATION PURPOSES)
! This is a driver program that calls functions from LinAl.f90 in order to perform
! Householder factorization (i.e. a QR decomposition) given matrix A and vector b
! to solve for x in the overdetermined system Ax=b. 

! COMPILATION INSTRUCTIONS: This driver program does NOT come with a Makefile. Therefore, to
! compile and run the program, type the following lines while in the part2 directory:
! gfortran -c  LinAl.f90 houseExampleDriver.f90
! gfortran -o houseExampleDriver LinAl.o houseExampleDriver.o
! ./houseExampleDriver

! Note: For the program to compile and run properly, it will also need the following files:
! LinAl.f90

! PROGRAM INPUTS:
! This program reqires no inputs, the matrix used is hard coded.

! PROGRAM OUTPUTS:
! This program will print several matrices to the console. First, it will print the
! A matrix and the b vector used. Then, it will perform a QR decomposition on A using
! the Householder method and print the resulting Q and R matrices.
! Next, it will calculate A - QR and Q^T Q - I print them.
! Finally, it will perform backsubstitution on the system Rx = Q^T b in order to solve for x,
! which will be the same x in the original system Ax = b, and print x. Finally, it will calculate
! Ax - b, where A and b are the original matrices, and x is the x vector
! computed using the least squares method, and it will print that norm.

program houseExampleDriver
	use LinAl, only: readAtkinson, printMat, euclideanNorm, householderFactorization, gaussBacksub, frobeniusNorm
	implicit none
	real :: normResult
    logical :: singular
	integer :: i, j
    real, dimension(6,3) :: exA, exAs, exQ
    real, dimension(6) :: exb, exbs
    real, dimension(3) :: exx 
    
    exA = reshape((/ 1, 0, 0, -1, -1, 0, 0, 1, 0, 1, 0, -1, 0, 0, 1, 0, 1, 1 /), shape(exA))
    exb = (/ 1237, 1941, 2417, 711, 1177, 475 /)
    exAs = exA
    exbs = exb
    
	print *, "A ="
    call printMat(exA, 6, 3)
	print *, "b ="
    call printMat(exb, 6, 1)
    
    call householderFactorization(exA, exQ, exb, 6, 3)
    
    print *, "R ="
	call printMat(exA(1:3, :), 3, 3)
    print *, "Q ="
	call printMat(exQ, 6, 3)
    
    print *, "A - QR ="
	call printMat(exAs - matmul(exQ, exA(1:3, :)), 6, 3)
    
    print *, "Q^T Q ="
	call printMat(matmul(transpose(exQ),exQ), 3, 3)
    
    print *, "Q^T b ="
    call printMat(exb, 6, 1)
    
    call gaussBacksub(exA(1:3, :), exb, 3, 1, singular)
    
    print *, "x ="
    call printMat(exb, 6, 1)
    
    print *, "Ax = "
    exb = matmul(exAs, exb(1:3))
    call printMat(exb, 6, 1)
    
    call euclideanNorm(exbs - exb, 6, normResult)
    print *, "error norm = ", normResult
	
end program houseExampleDriver