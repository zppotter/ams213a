! Zachary Potter
! zppotter@ucsc.edu
! AMS 213A
! eigenDriver.f90

! OVERVIEW:
! This is a driver program that calls functions from LinAl.f90 in order to perform
! many eigenvalue/eigenvector related actions. The program first computes the
! Hessenberg form of a symmetric matrix in order to obtain a tridiagonal matrix, then
! it uses the QR algorithm with and without shift to calculate the eigenvalues of a matrix,
! and finally, it uses the inverse iteration algorithm to calculate the eigenvectors of
! a matrix. 

! COMPILATION INSTRUCTIONS: This driver program comes with a Makefile. Therefore, to
! compile and run the program, type the following lines while in the part2 directory:
! make
! ./eigenDriver

! Note: For the program to compile and run properly, it will also need the following files:
! LinAl.f90
! Makefile

! PROGRAM INPUTS:
! This program does not require any inputs.

! PROGRAM OUTPUTS:
! This program will print several matrices to the console. First, it will print the matrix
! given in problem 1 of part 2 of the homework, and then it will compute the tridiagonal
! form of the matrix, which will also be printed. Then, it will print the matrix given in
! problem 2, and calculate the eigenvalues of the matrix by first using the QR algorithm
! without shift, and then by using the QR algorithm with shift and deflation. The eigenvalues
! will be printed to the screen both times. Finally, it will print the matrix given in
! problem 3 and calculate three of its eigenvectors using the inverse iteration algorithm
! and print those eigenvectors to the screen.

program eigenDriver
	use LinAl, only: printMat, hessenbergReduction, QRAlgWOShifts, QRAlgWithShifts, inverseIteration
	implicit none
	integer :: asize = 4, bsize = 3, csize = 4
	real, dimension(4,4) :: A
    real, dimension(3,3) :: B, Bs
    real, dimension(4,4) :: C, Cs
    real, dimension(4) :: eig
    integer :: i
    
    A = reshape((/ 5, 4, 1, 1, 4, 5, 1, 1, 1, 1, 4, 2, 1, 1, 2, 4 /), shape(A))
    B = reshape((/ 3, 1, 0, 1, 2, 1, 0, 1, 1 /), shape(B))
    C = reshape((/ 2, 1, 3, 4, 1, -3, 1, 5, 3, 1, 6, -2, 4, 5, -2, -1 /), shape(C))
	Bs = B
    Cs = C
	
    print *, "------------------------------ PROBLEM 1 ------------------------------"
    print *, "A ="
    call printMat(A, asize, asize)
    
    call hessenbergReduction(A, asize)
    
    print *, "Hessenberg form of A: "
    call printMat(A, asize, asize)
    
    print *, "------------------------------ PROBLEM 2 ------------------------------"
    print *, "B ="
    call printMat(B, bsize, bsize)
    
    call QRAlgWOShifts(B, bsize)
    
    print *, "Eigenvalues of B using QR algorithm without shifts:"
    do i = 1, bsize
        print *, B(i,i)
    end do
    
    B = Bs
    
    call QRAlgWithShifts(B, bsize)
    
    print *, "Eigenvalues of B using QR algorithm with shifts and deflation:"
    do i = 1, bsize
        print *, B(i,i)
    end do
    
    print *, "------------------------------ PROBLEM 3 ------------------------------"
    print *, "C ="
    call printMat(C, csize, csize)
    
    call inverseIteration(C, eig, csize, -8.0286)
    
    print *, "Eigenvector 1 ="
    call printMat(eig, csize, 1)
    
    C = Cs
    call inverseIteration(C, eig, csize, 7.9329)
    
    print *, "Eigenvector 2 ="
    call printMat(eig, csize, 1)
    
    C = Cs
    call inverseIteration(C, eig, csize, 5.6689)
    
    print *, "Eigenvector 3 ="
    call printMat(eig, csize, 1)
    
end program eigenDriver